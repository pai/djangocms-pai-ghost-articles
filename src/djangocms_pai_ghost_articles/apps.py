from django.apps import AppConfig


class DjangocmsPaiGhostArticlesConfig(AppConfig):
    name = 'djangocms_pai_ghost_articles'
